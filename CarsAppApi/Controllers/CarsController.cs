﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarsAppApi.Models;
using CarsAppApi.Services;
using CarsAppApi.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CarsAppApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CarsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly CarsService _carsService;
        
        public CarsController(IMapper mapper, CarsService carsService)
        {
            _mapper = mapper;
            _carsService = carsService;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            List<Car> cars = await _carsService.GetAllAsync();
            if (!cars.Any())
            {
                return NoContent();
            }
            
            IEnumerable<CarViewModel> carsViewModel = _mapper.Map<IEnumerable<Car>, IEnumerable<CarViewModel>>(cars);
            return Ok(carsViewModel);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            Car car = await _carsService.GetByIdAsync(id);
            if (car == null)
            {
                return NotFound();
            }
            
            CarViewModel carViewModel = _mapper.Map<Car, CarViewModel>(car);
            return Ok(carViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CarViewModel carViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Car car = _mapper.Map<CarViewModel, Car>(carViewModel);
            await _carsService.CreateAsync(car);

            return Created(Url.Action(nameof(Get), new {id = car.Id}), carViewModel);
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] CarViewModel carViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Car car = _mapper.Map<CarViewModel, Car>(carViewModel);
            await _carsService.UpdateAsync(car);

            return Ok(car);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            Car car = await _carsService.GetByIdAsync(id);
            if (car == null)
            {
                return NotFound();
            }

            await _carsService.DeleteAsync(car);
            return Ok();
        }
    }
}