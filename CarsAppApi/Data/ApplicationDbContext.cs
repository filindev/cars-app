﻿using CarsAppApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CarsAppApi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
            
        }
        
        public DbSet<Car> Cars { get; set; }

        public DbSet<Truck> Trucks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Car.Configuration.GetConfiguration(modelBuilder.Entity<Car>());
            Truck.Configuration.GetConfiguration(modelBuilder.Entity<Truck>());
        }
    }
}