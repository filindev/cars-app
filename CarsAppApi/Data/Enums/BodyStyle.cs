﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums
{
    public enum BodyStyle
    {
        [Display(Name = "Любой")]
        Any = 0,
        [Display(Name = "Седан")]
        Sedan = 1,
        [Display(Name = "Хэтчбэк")]
        Hatchback = 2,
        [Display(Name = "Лифтбэк")]
        Liftback = 3,
        [Display(Name = "Внедорожник")]
        SUV = 4,
        [Display(Name = "Универсал")]
        StationWagon = 5,
        [Display(Name = "Минивэн")]
        Minivan = 6,
        [Display(Name = "Микроавтобус")]
        Minibus = 7,
        [Display(Name = "Купе")]
        Coupe = 8,
        [Display(Name = "Фургон")]
        Van = 9,
        [Display(Name = "Пикап")]
        Pickup = 10,
        [Display(Name = "Кабриолет")]
        Cabriolet = 11,
        [Display(Name = "Лимузин")]
        Limousine = 12,
    }
}