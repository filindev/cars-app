﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums
{
    public enum CarBrand
    {
        [Display(Name = "Acura")]
        Acura,
        [Display(Name = "AlfaRomeo")]
        AlfaRomeo,
        [Display(Name = "ARO")]
        ARO,
        [Display(Name = "Asia")]
        Asia,
        [Display(Name = "AstonMartin")]
        AstonMartin,
        [Display(Name = "Audi")]
        Audi,
        [Display(Name = "Bentley")]
        Bentley,
        [Display(Name = "BMW")]
        BMW,
        [Display(Name = "Brilliance")]
        Brilliance,
        [Display(Name = "Buick")]
        Buick,
        [Display(Name = "BYD")]
        BYD,
        [Display(Name = "Cadillac")]
        Cadillac,
        [Display(Name = "Changan")]
        Changan,
        [Display(Name = "Chery")]
        Chery,
        [Display(Name = "Chevrolet")]
        Chevrolet,
        [Display(Name = "Chrysler")]
        Chrysler,
        [Display(Name = "Citroen")]
        Citroen,
        [Display(Name = "Dacia")]
        Dacia,
        [Display(Name = "Daewoo")]
        Daewoo,
        [Display(Name = "Daihatsu")]
        Daihatsu,
        [Display(Name = "Datsun")]
        Datsun,
        [Display(Name = "Derways")]
        Derways,
        [Display(Name = "Dodge")]
        Dodge,
        [Display(Name = "DongFeng")]
        DongFeng,
        [Display(Name = "FAW")]
        FAW,
        [Display(Name = "Ferrari")]
        Ferrari,
        [Display(Name = "Fiat")]
        Fiat,
        [Display(Name = "Ford")]
        Ford,
        [Display(Name = "FSO")]
        FSO,
        [Display(Name = "Geely")]
        Geely,
        [Display(Name = "GMC")]
        GMC,
        [Display(Name = "GreatWall")]
        GreatWall,
        [Display(Name = "Hafei")]
        Hafei,
        [Display(Name = "Haval")]
        Haval,
        [Display(Name = "Honda")]
        Honda,
        [Display(Name = "Hummer")]
        Hummer,
        [Display(Name = "Hyundai")]
        Hyundai,
        [Display(Name = "Infiniti")]
        Infiniti,
        [Display(Name = "IranKhodro")]
        IranKhodro,
        [Display(Name = "Isuzu")]
        Isuzu,
        [Display(Name = "JAC")]
        JAC,
        [Display(Name = "Jaguar")]
        Jaguar,
        [Display(Name = "Jeep")]
        Jeep,
        [Display(Name = "Jiangling")]
        Jiangling,
        [Display(Name = "Kia")]
        Kia,
        [Display(Name = "Lada")]
        Lada,
        [Display(Name = "Lancia")]
        Lancia,
        [Display(Name = "LandRover")]
        LandRover,
        [Display(Name = "Lexus")]
        Lexus,
        [Display(Name = "Lifan")]
        Lifan,
        [Display(Name = "Lincoln")]
        Lincoln,
        [Display(Name = "Mahindra")]
        Mahindra,
        [Display(Name = "Maserati")]
        Maserati,
        [Display(Name = "Mazda")]
        Mazda,
        [Display(Name = "McLaren")]
        McLaren,
        [Display(Name = "MercedesBenz")]
        MercedesBenz,
        [Display(Name = "Mercury")]
        Mercury,
        [Display(Name = "MG")]
        MG,
        [Display(Name = "MINI")]
        MINI,
        [Display(Name = "Mitsubishi")]
        Mitsubishi,
        [Display(Name = "Nissan")]
        Nissan,
        [Display(Name = "Oldsmobile")]
        Oldsmobile,
        [Display(Name = "Opel")]
        Opel,
        [Display(Name = "Peugeot")]
        Peugeot,
        [Display(Name = "Plymouth")]
        Plymouth,
        [Display(Name = "Pontiac")]
        Pontiac,
        [Display(Name = "Porsche")]
        Porsche,
        [Display(Name = "Proton")]
        Proton,
        [Display(Name = "Ravon")]
        Ravon,
        [Display(Name = "Renault")]
        Renault,
        [Display(Name = "Rover")]
        Rover,
        [Display(Name = "Saab")]
        Saab,
        [Display(Name = "Santana")]
        Santana,
        [Display(Name = "Saturn")]
        Saturn,
        [Display(Name = "Scion")]
        Scion,
        [Display(Name = "SEAT")]
        SEAT,
        [Display(Name = "Skoda")]
        Skoda,
        [Display(Name = "Smart")]
        Smart,
        [Display(Name = "SsangYong")]
        SsangYong,
        [Display(Name = "Subaru")]
        Subaru,
        [Display(Name = "Suzuki")]
        Suzuki,
        [Display(Name = "Tata")]
        Tata,
        [Display(Name = "Tesla")]
        Tesla,
        [Display(Name = "Toyota")]
        Toyota,
        [Display(Name = "Volkswagen")]
        Volkswagen,
        [Display(Name = "Volvo")]
        Volvo,
        [Display(Name = "Vortex")]
        Vortex,
        [Display(Name = "Wartburg")]
        Wartburg,
        [Display(Name = "Zotye")]
        Zotye,
        [Display(Name = "ZX")]
        ZX,
        [Display(Name = "ГАЗ")]
        GAZ,
        [Display(Name = "ЗАЗ")]
        ZAZ,
        [Display(Name = "ИЖ")]
        Izh,
        [Display(Name = "ЛуАЗ")]
        LuAZ,
        [Display(Name = "Москвич")]
        Moskvich,
        [Display(Name = "УАЗ")]
        UAZ,
        [Display(Name = "ТагАЗ")]
        TagAZ,
        [Display(Name = "Другие")]
        Other
    }
}