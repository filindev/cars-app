﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum ClimateAndHeating
    {
        [Display(Name = "Климат-контроль")]
        ClimateControl = 0,
        
        [Display(Name = "Кондиционер")]
        AirConditioning = 1,
        
        [Display(Name = "Обогрев зеркал")]
        HeatedMirrors = 2,
        
        [Display(Name = "Обогрев лобового стекла")]
        HeatedWindshield = 3,
        
        [Display(Name = "Обогрев руля")]
        SteeringWheelHeating = 4,
        
        [Display(Name = "Обогрев сидений")]
        SeatHeating = 5
    }
}