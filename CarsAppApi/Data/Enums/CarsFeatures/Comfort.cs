﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum Comfort
    {
        [Display(Name = "Автозапуск двигателя")]
        EngineStart = 0,
        
        [Display(Name = "Круиз-контроль")]
        CruiseControl = 1,
        
        [Display(Name = "Управление мультимедиа с руля")]
        MultimediaSteeringWheelControl = 2,
        
        [Display(Name = "Электрорегулировка сидений")]
        PowerSeats = 3,
        
        [Display(Name = "Электростеклоподъемники задние")]
        RearPowerWindows = 4,
        
        [Display(Name = "Электростеклоподъемники передние")]
        FrontPowerWindows = 5,
    }
}