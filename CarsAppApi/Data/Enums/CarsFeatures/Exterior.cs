﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum Exterior
    {
        [Display(Name = "Легкосплавные диски")]
        AlloyWheels = 0,
        
        [Display(Name = "Рейлинги на крыше")]
        RoofRails = 1,
        
        [Display(Name = "Фаркоп")]
        Hitch = 2
    }
}