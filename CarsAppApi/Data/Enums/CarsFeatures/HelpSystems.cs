﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum HelpSystems
    {
        [Display(Name = "Датчик дождя")]
        RainSensor = 0,
        
        [Display(Name = "Камера заднего вида")]
        RearViewCamera = 1,
        
        [Display(Name = "Контроль мертвых зон на зеркалах")]
        ControlDeadZones = 2,
        
        [Display(Name = "Парктроники")]
        ParkingSensors = 3
    }
}