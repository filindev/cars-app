﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum Interior
    {
        [Display(Name = "Люк")] 
        Luke = 0,
        
        [Display(Name = "Панорамная крыша")]
        RoofWithPanoramicView = 1
    }
}