﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum Multimedia
    {
        [Display(Name = "AUX/iPod")]
        AUX = 0,
        
        [Display(Name = "Bluetooth")]
        Bluetooth = 1,
        
        [Display(Name = "CD/MP3 проигрыватель")]
        CDPlayer = 2,
        
        [Display(Name = "USB")]
        USB = 3,
        
        [Display(Name = "Мультимедийный экран")]
        MultimediaScreen = 4,
        
        [Display(Name = "Штатная навигация")]
        NativeNavigation = 5
    }
}