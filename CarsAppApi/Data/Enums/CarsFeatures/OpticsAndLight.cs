﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum OpticsAndLight
    {
        [Display(Name = "Ксеноновые фары")]
        XenonHeadlights = 0,
        
        [Display(Name = "Противотуманные фары")]
        FogLights = 1,
        
        [Display(Name = "Светодиодные фары")]
        LEDHeadlights = 2
    }
}