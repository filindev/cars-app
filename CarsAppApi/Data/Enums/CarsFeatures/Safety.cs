﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums.CarsFeatures
{
    public enum Safety
    {
        [Display(Name = "Антиблокировочная система")]
        ABS = 0,

        [Display(Name = "Система поддержания динамической стабильности")]
        ESP = 1,

        [Display(Name = "Антипробуксовочная система")]
        TractionControl = 2,
        
        [Display(Name = "")]
        Immobilizer = 3,

        [Display(Name = "Боковые подушки безопасности")]
        SideAirbags = 4,

        [Display(Name = "Задние подушки безопасности")]
        RearAirbags = 5,

        [Display(Name = "Передние подушки безопасности")]
        FrontAirbags = 6,
        
        [Display(Name = "Сигнализация")] 
        Signaling = 7
    }
}