﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums
{
    public enum Color
    {
        [Display(Name = "Другой")]
        Any = 0,
        [Display(Name = "Белый")]
        White = 1,
        [Display(Name = "Бордовый")]
        Burgundy = 2,
        [Display(Name = "Желтый")]
        Yellow = 3,
        [Display(Name = "Зеленый")]
        Green = 4,
        [Display(Name = "Коричневый")]
        Brown = 5,
        [Display(Name = "Красный")]
        Red = 6,
        [Display(Name = "Оранжевый")]
        Orange = 7,
        [Display(Name = "Серебристый")]
        Silver = 8,
        [Display(Name = "Серый")]
        Gray = 9,
        [Display(Name = "Синий")]
        Blue = 10,
        [Display(Name = "Фиолетовый")]
        Violet = 11,
        [Display(Name = "Черный")]
        Black = 12
    }
}