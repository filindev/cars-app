﻿namespace CarsAppApi.Data.Enums
{
    public enum Currency
    {
        BYN,
        USD,
        EUR,
        RUB
    }
}