﻿using System.ComponentModel.DataAnnotations;

namespace CarsAppApi.Data.Enums
{
    public enum Drive
    {
        [Display(Name = "Любой")]
        Any = 0,
        [Display(Name = "Передний")]
        FWD = 1,
        [Display(Name = "Задний")]
        RWD = 2,
        [Display(Name = "Подключаемый полный")]
        AWDOnDemand = 3,
        [Display(Name = "Постоянный полный")]
        AWDFullTime = 4
    }
}