﻿namespace CarsAppApi.Data.Enums
{
    public enum Fuel
    {
        Any = 0,
        Gasoline = 1,
        Diesel = 2
    }
}