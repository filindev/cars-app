﻿namespace CarsAppApi.Data.Enums
{
    public enum Transmission
    {
        Any = 0,
        Automatic = 1,
        Manual = 2,
        Automanual = 3,
        CVT = 4
    }
}