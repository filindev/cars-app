﻿using AutoMapper;
using CarsAppApi.Models;
using CarsAppApi.ViewModels;

namespace CarsAppApi.Mappings
{
    public class CarMappingProfile : Profile
    {
        public CarMappingProfile()
        {
            CreateMap<Car, CarViewModel>()
                .ReverseMap();
        }
    }
}