﻿using CarsAppApi.Data.Enums;

namespace CarsAppApi.Models.Base
{
    public abstract class Automobile : Vehicle
    {
        public CarBrand Brand { get; set; }
        
        public Fuel Fuel { get; set; }
        
        public Drive Drive { get; set; }
        
        public Transmission Transmission { get; set; }
        
        public BodyStyle BodyStyle { get; set; }
        
        public int Year { get; set; }
        
        public int Mileage { get; set; }
        
        public int EngineDisplacement { get; set; } // in centimeters
        
        public Color Color { get; set; }
    }
}