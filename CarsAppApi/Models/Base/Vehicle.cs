﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CarsAppApi.Data.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace CarsAppApi.Models.Base
{
    public abstract class Vehicle : BaseDbModel<int>
    {
        public string Description { get; set; }
        
        public int ViewsCount { get; set; }

        private string _currency
        {
            get => JsonConvert.SerializeObject(Currency);
            set => Currency = JsonConvert.DeserializeObject<Currency>(value);
        }
        
        [NotMapped]
        public Currency Currency { get; set; }

        private string _media
        {
            get => JsonConvert.SerializeObject(Media);
            set => Media = JsonConvert.DeserializeObject<List<Media>>(value);
        }

        [NotMapped]
        public List<Media> Media { get; set; } = new List<Media>();
        
        public class VehicleConfiguration<TModel> where TModel : Vehicle
        {
            protected virtual void Configure(EntityTypeBuilder<TModel> builder)
            {
                builder.Property(m => m._media).HasDefaultValue("[]");
                builder.Property(m => m._currency).HasDefaultValue("{}");
            }
        }
    }
}