﻿using System;

namespace CarsAppApi.Models
{
    public abstract class BaseDbModel<TId> where TId : struct
    {
        public TId Id { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
        
        public DateTime Updated { get; set; } = DateTime.UtcNow;
    }
}