﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CarsAppApi.Models.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace CarsAppApi.Models
{
    public class Car : Automobile
    {
        private string _carsFeatures
        {
            get => JsonConvert.SerializeObject(CarsFeatures);
            set => CarsFeatures = JsonConvert.DeserializeObject<CarsFeatures>(value);
        }
        
        [NotMapped]
        public CarsFeatures CarsFeatures { get; set; }
        
        public class Configuration : VehicleConfiguration<Car>
        {
            protected override void Configure(EntityTypeBuilder<Car> builder)
            {
                base.Configure(builder);

                builder.Property(m => m._carsFeatures).HasDefaultValue("{}");
            }

            public static void GetConfiguration(EntityTypeBuilder<Car> builder)
            {
                Configuration configuration = new Configuration();
                configuration.Configure(builder);
            }
        }
    }
}