﻿using System.Collections.Generic;
using CarsAppApi.Data.Enums.CarsFeatures;
using Newtonsoft.Json;

namespace CarsAppApi.Models
{
    public class CarsFeatures
    {
        [JsonProperty]
        public IEnumerable<ClimateAndHeating> ClimateAndHeatingFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<Comfort> ComfortFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<Exterior> ExteriorFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<HelpSystems> HelpSystemsFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<Interior> InteriorFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<Multimedia> MultimediaFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<OpticsAndLight> OpticsAndLightFeatures { get; set; }
        
        [JsonProperty]
        public IEnumerable<Safety> SafetyFeatures { get; set; }
    }
}