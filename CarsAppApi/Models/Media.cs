﻿namespace CarsAppApi.Models
{
    public class Media
    {
        public string Id { get; set; }

        public string Path { get; set; }
        
        public bool IsTemp { get; set; }
        
        public bool IsCover { get; set; }
    }
}