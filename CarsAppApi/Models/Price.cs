﻿using CarsAppApi.Data.Enums;

namespace CarsAppApi.Models
{
    public class Price
    {
        public int Amount { get; set; }
        
        public Currency Currency { get; set; }
    }
}