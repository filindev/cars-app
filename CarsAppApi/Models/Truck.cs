﻿using CarsAppApi.Models.Base;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarsAppApi.Models
{
    public class Truck : Automobile
    {
        public class Configuration : VehicleConfiguration<Truck>
        {
            public static void GetConfiguration(EntityTypeBuilder<Truck> builder)
            {
                Configuration configuration = new Configuration();
                configuration.Configure(builder);
            }
        }
    }
}