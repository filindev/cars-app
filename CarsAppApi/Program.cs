﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace CarsAppApi
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(AddConfigurationFiles)
                .UseStartup<Startup>();

        private static void AddConfigurationFiles(WebHostBuilderContext context, IConfigurationBuilder builder)
        {
            builder.AddJsonFile("config.json");
            if (context.HostingEnvironment.IsDevelopment())
            {
                builder.AddJsonFile("config.local.json");
            }
        }
    }
}