﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarsAppApi.Data;
using CarsAppApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CarsAppApi.Services
{
    public abstract class BaseDbService<TModel, TId>
        where TModel : BaseDbModel<TId>
        where TId : struct
    {
        private readonly DbSet<TModel> _dbSet;
        private readonly ApplicationDbContext _context;
        
        protected BaseDbService(ApplicationDbContext context)
        {
            _dbSet = context.Set<TModel>();
            _context = context;
        }
        
        public async Task<TModel> GetByIdAsync(TId id)
        {
            return await _dbSet.FindAsync(id);
        }
        
        public async Task<List<TModel>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }
        
        public async Task CreateAsync(TModel model)
        {
            await _dbSet.AddAsync(model);
            await _context.SaveChangesAsync();
        }
        
        public async Task UpdateAsync(TModel model)
        {
            _dbSet.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TModel model)
        {
            _dbSet.Remove(model);
            await _context.SaveChangesAsync();
        }
    }
}