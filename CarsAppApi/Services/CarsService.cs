﻿using CarsAppApi.Data;
using CarsAppApi.Models;

namespace CarsAppApi.Services
{
    public class CarsService : BaseDbService<Car, int>
    {
        public CarsService(ApplicationDbContext context) : base(context)
        {
        }
    }
}